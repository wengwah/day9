-----------------------------------
Steps to prepare files for the day
-----------------------------------

## Prepare Server folder

## Prepare Client Folder

## Prepare Git 
  * Initialize git on folder ```git init``` 
  * Create remote repository in bitbucket (Plus sign on bitbucket)
  * Add remote add origin by pasting over code ```git remote add origin (http://bitbucketlink here)```
  * Git add to staging area ```git add .```
  * Git commit to local repository ```git commit -m "message here"```
  * Git push to remote repository ```git push origin master -u```

## Prepare Dependencies
  * Prepare scafolding aka package.JSON file ```npm init```
  * Download and install express ```npm install express --save```
  * Download and install body-parser ```npm install body-parser --save```

## Bower 
  * Initialize bower ```bower init```
  * Download bower globally with -g (This handles front end modules and dependencies) ```npm install -g bower```
  * Create a .bowerrc file in the root directory to point to the client folder for installing front-end modules (refer to file for path) ```touch .bowerrc```
  * Download and install Angular ```bower install angular --save```
  * Download and install Bootstrap ```bower install bootstrap --save```
  * Download and install Font Awesome ```bower install font-awesome --save```

## Starting the app (Console)
  * ```export NODE_PORT=3000```
  * ```nodemon``` or ```nodemon server/app.js```

## Launching the app on the browser
  * To launch the page, open the browser ```http://localhost:3000```
  * To examine the endpoints ```http://localhost:3000/users``` for instance.

## Launching an app from a git clone copy
  * ```git clone```
  * Install all node packages```npm install```

## Additional steps for this app
  * ```bower install --save angular-animate```
  * Install all bower packages ```bower install```